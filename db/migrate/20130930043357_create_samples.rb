class CreateSamples < ActiveRecord::Migration
  def change
    create_table :samples do |t|
      t.integer :userid
      t.string :kind
      t.string :measure
      t.string :units
      t.float :value

      t.timestamps
    end
  end
end
