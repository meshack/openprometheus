json.array!(@samples) do |sample|
  json.extract! sample, :userid, :kind, :measure, :units, :value
  json.url sample_url(sample, format: :json)
end
