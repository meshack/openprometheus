class Task < ActiveRecord::Base
  belongs_to :sample
  has_many :results, dependent: :destroy
end
